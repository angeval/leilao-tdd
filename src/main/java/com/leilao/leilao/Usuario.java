package com.leilao.leilao;

public class Usuario {

    private Integer id;
    private String nome;

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }
}
