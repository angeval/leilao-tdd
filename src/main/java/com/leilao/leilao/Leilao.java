package com.leilao.leilao;

import java.util.List;

public class Leilao {

    private List<Lance> lances;
    public final double valorinicial = 0;

    public Lance adicionarNovoLance(Lance lance){
        if(validarLance(lance)){
            lances.add(lance);
        }
        return lance;
    }

    public double getValorinicial() {
        return valorinicial;
    }

    public Boolean validarLance(Lance lance){
        List<Lance> lances = getListaLance();
        if(lance.getValor() <= valorinicial){
            return false;
        }
        if(lances.isEmpty() || lance.getValor() > lances.get(lances.size() - 1).getValor())
        { return true; }
        else
        { return false; }
    }

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public List<Lance> getListaLance() {
        return lances;
    }

    public void setListaLance(List<Lance> lances) {
        this.lances = lances;
    }
}
