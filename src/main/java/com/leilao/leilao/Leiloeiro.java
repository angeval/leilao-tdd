package com.leilao.leilao;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Lance retornaMaiorLance() {
        Lance lance = null;
        if(!leilao.getListaLance().isEmpty()){
            lance = leilao.getListaLance().get(leilao.getListaLance().size() - 1);}
        return lance;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }
}
