package com.leilao.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.naming.directory.InvalidAttributeValueException;
import javax.naming.directory.InvalidAttributesException;
import java.util.ArrayList;
import java.util.List;

public class LeilaoTests {

    @Test
    public void testarValidarPrimeiroLance(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 1);
        Assertions.assertEquals(leilao.validarLance(lance), true);
    }

    @Test
    public void testarValidarNovoLanceMaior(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Lance l1 = new Lance(usuario, 1);
        Lance l2 = new Lance(usuario, 2);
        Lance l3 = new Lance(usuario, 3);
        lances.add(l1);
        lances.add(l2);
        lances.add(l3);
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 4);
        Assertions.assertEquals(leilao.validarLance(lance), true);
    }

    @Test
    public void testarValidarNovoLanceMenor(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Lance l1 = new Lance(usuario, 1);
        Lance l2 = new Lance(usuario, 2);
        Lance l3 = new Lance(usuario, 6);
        lances.add(l1);
        lances.add(l2);
        lances.add(l3);
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 5);
        Assertions.assertEquals(leilao.validarLance(lance), false);
    }

    @Test
    public void testarValidarPrimeiroLanceZero(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 0);
        Assertions.assertEquals(leilao.validarLance(lance), false);
    }

    @Test
    public void testarAdicionarNovoLanceZeroFalse(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 0);
        leilao.adicionarNovoLance(lance);
        Assertions.assertFalse(leilao.getListaLance().contains(lance));
    }

    @Test
    public void testarAdicionarNovoPrimeiroLanceTrue(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 1);
        leilao.adicionarNovoLance(lance);
        Assertions.assertTrue(leilao.getListaLance().contains(lance));
    }

    @Test
    public void testarAdicionarNovoLanceFalse(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Lance l1 = new Lance(usuario, 1);
        Lance l2 = new Lance(usuario, 2);
        Lance l3 = new Lance(usuario, 6);
        lances.add(l1);
        lances.add(l2);
        lances.add(l3);
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 5);
        leilao.adicionarNovoLance(lance);
        Assertions.assertFalse(leilao.getListaLance().contains(lance));
    }

    @Test
    public void testarAdicionarNovoLanceTrue(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Lance l1 = new Lance(usuario, 1);
        Lance l2 = new Lance(usuario, 2);
        Lance l3 = new Lance(usuario, 6);
        lances.add(l1);
        lances.add(l2);
        lances.add(l3);
        Leilao leilao = new Leilao(lances);
        Lance lance = new Lance(usuario, 10);
        leilao.adicionarNovoLance(lance);
        Assertions.assertTrue(leilao.getListaLance().contains(lance));
    }

    @Test
    public void testarRetornarMaiorValor(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Lance l1 = new Lance(usuario, 1);
        Lance l2 = new Lance(usuario, 2);
        Lance l3 = new Lance(usuario, 6);
        lances.add(l1);
        lances.add(l2);
        lances.add(l3);
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Jorge", leilao);
        Assertions.assertEquals(leiloeiro.retornaMaiorLance().getValor(), 6);
    }

    @Test
    public void testarRetornarMaiorValorComUm(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Lance l1 = new Lance(usuario, 1);
        lances.add(l1);
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Jorge", leilao);
        Assertions.assertEquals(leiloeiro.retornaMaiorLance().getValor(), 1);
    }

    @Test
    public void testarRetornarMaiorValorComListaVazia(){
        Usuario usuario = new Usuario(1, "Angela");
        List<Lance> lances = new ArrayList<Lance>();
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Jorge", leilao);
        Assertions.assertThrows(NullPointerException.class, () -> {leiloeiro.retornaMaiorLance().getValor();});
    }

    //@Test
    //public void testarLanceInvalido(){
    //    Usuario usuario = new Usuario(1, "Angela");
    //    Lance lance = new Lance(usuario, 0);
    //    List<Lance> lances = new ArrayList<Lance>();
    //    Leilao leilao = new Leilao(lances);
    //    Caso o método de leilão retorne uma exceção para o método de adicionar novo lance
    //    Assertions.assertThrows(InvalidAttributeValueException.class, () -> {leilao.adicionarNovoLance(lance);});
    //}
}
